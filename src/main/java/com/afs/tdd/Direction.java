package com.afs.tdd;

public enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST;

    public Direction getRightDirection() {
        return values()[(this.ordinal() + 1) % 4];
    }

    public Direction getLeftDirection() {
        return values()[(this.ordinal() + 4 - 1) % 4];
    }
}
