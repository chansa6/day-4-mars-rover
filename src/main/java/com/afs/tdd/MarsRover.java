package com.afs.tdd;

import java.util.Arrays;

public class MarsRover {
    private Position roverPosition;

    public MarsRover(Position roverPosition) {
        this.roverPosition = roverPosition;
    }


    public Position executeCommands(String commands) {
        Position finalPosition = Arrays.stream(commands.split("")).
                reduce(roverPosition, this::executeCommand, (pos1, pos2) -> pos2);
        setRoverPosition(finalPosition);
        return finalPosition;
    }


    public Position executeCommand(Position currentPosition, String command) {
        return switch(command) {
            case "M" -> moveForward(currentPosition);
            case "L" -> turnLeft(currentPosition);
            case "R" -> turnRight(currentPosition);
            default -> currentPosition;
        };
    }

    private Position turnRight(Position roverPosition) {
        Direction currentDirection = roverPosition.getDirection();
        return new Position(roverPosition.getX(), roverPosition.getY(), currentDirection.getRightDirection());
    }

    private Position turnLeft(Position roverPosition) {
        Direction currentDirection = roverPosition.getDirection();
        return new Position(roverPosition.getX(), roverPosition.getY(), currentDirection.getLeftDirection());

    }

    private Position moveForward(Position roverPosition) {
        Direction currentDirection = roverPosition.getDirection();

        return switch (currentDirection) {
            case NORTH -> new Position(roverPosition.getX(), roverPosition.getY() + 1, roverPosition.getDirection());
            case SOUTH -> new Position(roverPosition.getX(), roverPosition.getY() - 1, roverPosition.getDirection());
            case EAST -> new Position(roverPosition.getX() + 1, roverPosition.getY(), roverPosition.getDirection());
            case WEST -> new Position(roverPosition.getX() - 1, roverPosition.getY(), roverPosition.getDirection());
        };

    }

    public void setRoverPosition(Position roverPosition) {
        this.roverPosition = roverPosition;
    }
}
