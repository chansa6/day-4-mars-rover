package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarsRoverTest {
    // === M tests ===
    @Test
    void should_return_0_1_N_when_M_given_0_0_N() {
        // given
        Position initialPosition = new Position(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, 1, Direction.NORTH);

        // when
        Position actualPosition = rover.executeCommand(initialPosition, "M");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_0_neg1_S_when_M_given_0_0_S() {
        // given
        Position initialPosition = new Position(0, 0, Direction.SOUTH);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, -1, Direction.SOUTH);

        // when
        Position actualPosition = rover.executeCommand(initialPosition, "M");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_1_0_E_when_M_given_0_0_E() {
        // given
        Position initialPosition = new Position(0, 0, Direction.EAST);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(1, 0, Direction.EAST);

        // when
        Position actualPosition = rover.executeCommand(initialPosition, "M");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_neg1_0_W_when_M_given_0_0_W() {
        // given
        Position initialPosition = new Position(0, 0, Direction.WEST);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(-1, 0, Direction.WEST);

        // when
        Position actualPosition = rover.executeCommand(initialPosition, "M");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    // === L tests ===
    @Test
    void should_return_0_0_W_when_L_given_0_0_N() {
        // given
        Position initialPosition = new Position(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, 0, Direction.WEST);

        // when
        Position actualPosition = rover.executeCommand(initialPosition, "L");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_0_0_E_when_L_given_0_0_S() {
        // given
        Position initialPosition = new Position(0, 0, Direction.SOUTH);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, 0, Direction.EAST);

        // when
        Position actualPosition = rover.executeCommand(initialPosition, "L");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_0_0_N_when_L_given_0_0_E() {
        // given
        Position initialPosition = new Position(0, 0, Direction.EAST);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, 0, Direction.NORTH);


        // when
        Position actualPosition = rover.executeCommand(initialPosition, "L");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_0_0_S_when_L_given_0_0_W() {
        // given
        Position initialPosition = new Position(0, 0, Direction.WEST);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, 0, Direction.SOUTH);


        // when
        Position actualPosition = rover.executeCommand(initialPosition, "L");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    // === R tests ===
    @Test
    void should_return_0_0_E_when_R_given_0_0_N() {
        // given
        Position initialPosition = new Position(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, 0, Direction.EAST);


        // when
        Position actualPosition = rover.executeCommand(initialPosition, "R");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_0_0_W_when_R_given_0_0_S() {
        // given
        Position initialPosition = new Position(0, 0, Direction.SOUTH);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, 0, Direction.WEST);


        // when
        Position actualPosition = rover.executeCommand(initialPosition, "R");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_0_0_S_when_R_given_0_0_E() {
        // given
        Position initialPosition = new Position(0, 0, Direction.EAST);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, 0, Direction.SOUTH);


        // when
        Position actualPosition = rover.executeCommand(initialPosition, "R");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_0_0_N_when_R_given_0_0_W() {
        // given
        Position initialPosition = new Position(0, 0, Direction.WEST);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, 0, Direction.NORTH);


        // when
        Position actualPosition = rover.executeCommand(initialPosition,"R");

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    // === Stream tests ===
    @Test
    void should_return_neg1_0_W_when_M_stream_given_0_0_W() {
        // given
        Position initialPosition = new Position(0, 0, Direction.WEST);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(-1, 0, Direction.WEST);

        // when
        Position actualPosition = rover.executeCommands("M");
        actualPosition.printPosition();

        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_0_2_N_when_MM_given_0_0_N() {
        // given
        Position initialPosition = new Position(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(0, 2, Direction.NORTH);


        // when
        Position actualPosition = rover.executeCommands("MM");
        actualPosition.printPosition();


        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_0_2_N_when_MMMLMMM_given_0_0_N() {
        // given
        Position initialPosition = new Position(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(-3, 3, Direction.WEST);


        // when
        Position actualPosition = rover.executeCommands("MMMLMMM");
        actualPosition.printPosition();


        // then
        assertEquals(expectedPosition, actualPosition);
    }

    @Test
    void should_return_2_0_S_when_RMLMLMLMLMMR_given_0_0_N() {
        // given
        Position initialPosition = new Position(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(initialPosition);
        Position expectedPosition = new Position(2, 0, Direction.SOUTH);


        // when
        Position actualPosition = rover.executeCommands("RMLMLMLMLMMR");
        actualPosition.printPosition();


        // then
        assertEquals(expectedPosition, actualPosition);
    }




}
